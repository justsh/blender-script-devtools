bl_info = {
    "name": "Script Helper",
    "author": "Justin Sheehy",
    "version": (1, 0),
    "blender": (2, 6, 7),
    "location": "TEXT_EDITOR > Properties",
    "description": "Compiles and runs external scripts in blender",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Development"}


import bpy
from bpy.props import PointerProperty
from bpy.props import IntProperty
from bpy.props import StringProperty
from bpy.props import BoolProperty
from bpy.props import CollectionProperty

import os


class ScriptHelperFile(bpy.types.PropertyGroup):

    @classmethod
    def register(cls):

        cls.filepath = StringProperty(
            name="File Path",
            description="Filesystem location of the script",
            maxlen=1024,
            subtype='FILE_PATH'
        )


class ScriptHelperSettings(bpy.types.PropertyGroup):

    @classmethod
    def register(cls):

        bpy.types.Scene.script_helper = PointerProperty(
            name="Script Helper Settings",
            description="Script Helper Settings",
            type=cls)

        cls.scripts = CollectionProperty(
            type=ScriptHelperFile,
            name="Scripts",
            description="The set of loaded external scripts"
        )

        cls.active_script = PointerProperty(
            name="Active Script",
            type=ScriptHelperFile,
            description="Not Implemented: Pointer to the active script"
        )

        cls.active_panel_index = IntProperty(
            name="Panel Index",
            description="Index of the active script in the Panel",
            default=-1
        )

        cls.active_menu_index = IntProperty(
            name="Menu Index",
            description="Index of the active script in the Menu",
            default=-1
        )

        cls.replace_selection = BoolProperty(
            name="Replace Selected Script?",
            description="Always ask for a new script when running on the "
                        "active list selection",
            default=False
        )

    @classmethod
    def unregister(cls):
        del bpy.types.Scene.script_helper


class TEXT_UL_script_helper(bpy.types.UIList):

    def draw_item(self, context, layout,
                  data, item, icon, active_data, active_propname, index):
        script = item
        t = ""

        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            name = os.path.splitext(
                os.path.basename(script.filepath))[0].replace('_', ' ').title()
            txt = ''.join([name, ":  ", script.name])
            layout.label(text=txt, translate=False, icon='FILE_SCRIPT')

        elif self.layout_type in {'GRID'}:
            layout.alignment = 'CENTER'
            layout.label(text="", icon_value=icon)


class ScriptHelperMenu(bpy.types.Menu):
    bl_label = "Script Helper Menu"
    bl_idname = "TEXT_MT_script_helper"

    def draw(self, context):
        helper = context.scene.script_helper
        layout = self.layout

        col = layout.column(align=True)

        for script in helper.scripts:
            col.operator("script_helper.run_active",
                         text=script.name,
                         icon='FILE_SCRIPT').script = script.filepath

        # layout.template_list("TEXT_UL_script_helper",
        #                      "script_helper_files",
        #                      helper,
        #                      "scripts",
        #                      helper,
        #                      "active_menu_index",
        #                      type='DEFAULT')


class ScriptHelperPanel(bpy.types.Panel):
    bl_label = "Script Helper"
    bl_idname = "TEXT_PT_script_helper"
    bl_space_type = "TEXT_EDITOR"
    bl_region_type = "UI"
    bl_context = ""

    def draw(self, context):
        helper = context.scene.script_helper
        layout = self.layout

        col = layout.column()
        row = col.row()

        row.template_list("TEXT_UL_script_helper",
                          "script_helper_files",
                          helper,
                          "scripts",
                          helper,
                          "active_panel_index",
                          rows=3,
                          maxrows=10)

        col = row.column(align=True)
        col.operator("script_helper.add_script", icon='ZOOMIN', text="")
        col.operator("script_helper.remove_script", icon='ZOOMOUT', text="")

        row = layout.row(align=True)
        row.template_ID(helper,
                        "active_script")

        col = layout.column()
        col.separator()

        col.prop(helper, 'replace_selection')

        col.separator()
        col.separator()

        col.operator('script_helper.run_active', text="Run Active Script")


class ScriptHelperAddScript(bpy.types.Operator):
    """Add a filepath to the list of external scripts"""
    bl_idname = "script_helper.add_script"
    bl_label = "Add Script"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        helper = context.scene.script_helper

        if not helper.replace_selection:
            helper.scripts.add()
            helper.active_panel_index = len(helper.scripts.items()) - 1

        bpy.ops.script_helper.browse('INVOKE_DEFAULT')

        return {'FINISHED'}

    def invoke(self, context, event):
        return self.execute(context)


class ScriptHelperRemoveScript(bpy.types.Operator):
    """Add a filepath to the list of external scripts"""
    bl_idname = "script_helper.remove_script"
    bl_label = "Remove Script"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        helper = context.scene.script_helper

        if helper.active_panel_index > -1:

            helper.scripts.remove(helper.active_panel_index)
            helper.active_panel_index -= 1

        return {'FINISHED'}

    def invoke(self, context, event):
        return self.execute(context)


class ScriptHelperBrowse(bpy.types.Operator):
    """Browses for a script file to use with the Script Helper"""
    bl_idname = "script_helper.browse"
    bl_label = "Accept"

    filepath = StringProperty(
        name="File Path",
        description="Filepath to load the script",
        maxlen=1024,
        subtype='FILE_PATH'
    )

    filter_folder = BoolProperty(
        name="Filter folders",
        default=True,
        options={'HIDDEN'}
    )
    filter_text = BoolProperty(
        name="Filter text",
        default=True,
        options={'HIDDEN'}
    )
    filter_python = BoolProperty(
        name="Filter python",
        default=True,
        options={'HIDDEN'}
    )

    def execute(self, context):
        helper = context.scene.script_helper

        filepath = self.filepath
        filepath = bpy.path.abspath(filepath)
        filepath = os.path.normpath(filepath)

        active = helper.scripts[helper.active_panel_index]
        active.filepath = filepath
        active.name = bpy.path.relpath(filepath)
        return {'FINISHED'}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class ScriptHelperRunActive(bpy.types.Operator):
    """Compiles and Executes an external script in Blender"""
    bl_idname = "script_helper.run_active"
    bl_label = "Execute Script"

    script = StringProperty(
        name="Script",
        description="Force a given script to run",
        default="",
        options={"HIDDEN"}
    )

    def execute(self, context):

        if self.script:
            self.run_script(self.script)
            return {'FINISHED'}

        helper = context.scene.script_helper
        active = helper.scripts[helper.active_panel_index]

        if (not active.filepath or helper.replace_selection):
            bpy.ops.script_helper.browse('INVOKE_DEFAULT')

        self.run_script(active.filepath)
        return {'FINISHED'}

    def run_script(self, filepath):
        code_obj = self.compile_script(filepath)

        global_namespace = {"__file__": filepath, "__name__": "__main__"}
        exec(code_obj, global_namespace)

        del(code_obj)
        self.report({"INFO"}, "Script Completed: {0}".format(filepath))

    def compile_script(self, filepath):

        filepath = bpy.path.abspath(filepath)
        contents = None

        try:
            with open(filepath, 'rb') as f:
                contents = f.read()
        except IOError:
            # Blender will report this itself
            raise
        except UnboundLocalError:
            print("Caught unbounded")
            raise

        return compile(contents, filepath, 'exec')


def register():
    bpy.utils.register_module(__name__)

    # Register Keymaps
    kc = bpy.context.window_manager.keyconfigs.addon

    if kc:
        km = (kc.keymaps.new(name="Screen", space_type="EMPTY")
              if 'Screen' not in kc.keymaps
              else kc.keymaps['Screen'])

        kmi = km.keymap_items.new('script_helper.run_active',
                                  'F8',
                                  'PRESS',
                                  ctrl=True)

        kmi = km.keymap_items.new('wm.call_menu',
                                  'F8',
                                  'PRESS',
                                  shift=True,
                                  ctrl=True)
        kmi.properties.name = "TEXT_MT_script_helper"


def unregister():
    bpy.utils.unregister_module(__name__)

    # Delete extra keymaps
    kc = bpy.context.window_manager.keyconfigs.addon
    if kc:
        km = kc.keymaps["Screen"]
        for kmi in km.keymap_items:
            if kmi.idname == 'script_helper.run_active':
                km.keymap_items.remove(kmi)
            elif kmi.idname == 'wm.call_menu':
                if kmi.properties.name == "TEXT_MT_script_helper":
                    km.keymap_items.remove(kmi)
